<?php
    require_once 'vendor/autoload.php';

    $loader = new Twig_Loader_Filesystem('views/');
    $twig = new Twig_Environment($loader);

    $current_date = new GetTime();

    echo $twig->render('page.html', [
        'greatings' => 'Hello, User!',
        'date' => $current_date->getDate(),
        'user_ip' => $_SERVER['REMOTE_ADDR'],
    ]);